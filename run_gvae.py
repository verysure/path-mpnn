import os
import torch
from torch import nn
import modules.moldataset as md
import modules.graph3d as g3d
import modules.utils as ut
from tqdm import tqdm
import numpy as np


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')



class SimpleGraphConv(nn.Module):
    def __init__(self, node_dim, edge_dim, out_dim):
        super().__init__()
        self.graph_conv = g3d.GraphConv3D(
            edge_module = g3d.NodeEdgeNode3D(node_dim, edge_dim, out_dim),
            message_module = g3d.EdgePassThrough(),
            combine_module = g3d.Attention(out_dim, dim = 2, mean=False),
            # combine_module = g3d.Mean(dim = 2),
            update_module = g3d.GRUUpdate(node_dim, out_dim),
            )
    def forward(self, X):
        return self.graph_conv(X)

class SimpleGraphTranspose(nn.Module):
    def __init__(self, node_dim, edge_dim, out_dim):
        super().__init__()
        self.graph_conv = g3d.GraphConv3D(
            edge_module = g3d.NodeEdgeNode(node_dim, edge_dim, out_dim),
            message_module = g3d.EdgePassThrough(),
            combine_module = g3d.Attention(out_dim, dim = 2, mean=False),
            # combine_module = g3d.Mean(dim = 2),
            update_module = g3d.GRUUpdate(node_dim, out_dim),
            )
    def forward(self, X):
        return self.graph_conv(X)


class GVAE(nn.Module):
    def __init__(self, node_dim, edge_dim, latents = 15, dist_encoding = None):
        super().__init__()

        self.dist_enc = dist_encoding
        edge_dim = edge_dim + dist_encoding[0]

        # with edge update
        self.graphconv = nn.Sequential(
            SimpleGraphConv(node_dim, edge_dim, 64), 
            SimpleGraphConv(64, 64, 128),
            SimpleGraphConv(128, 128, 256),)

        hidden_size = 512
        self.readout = g3d.Set2Vec(256, hidden_size, dim=1, steps=5)
        self.out_mu = nn.Sequential( nn.Linear(hidden_size, latents),)
        self.out_logvar = nn.Sequential( nn.Linear(hidden_size, latents),)
        # need to have dual network

        
        self.linr = nn.Sequential(nn.Linear(latents, hidden_size), nn.Tanh())
        self.decompress = g3d.Vec2Set(hidden_size, 256, expand=29) 

        self.graphtranspose = nn.Sequential(
            SimpleGraphTranspose(256, 1, 128), 
            SimpleGraphTranspose(128, 128, 64),
            SimpleGraphTranspose(64, 64, 64),)

        self.n_out = nn.Sequential(nn.Linear(64, 1+node_dim), nn.Softmax(dim=-1))
        self.e_out = nn.Sequential(nn.Linear(64, 1+edge_dim-dist_encoding[0]), nn.Softmax(dim=-1))



    def forward(self, x):
        z = self.encode(x)
        return self.decode(z)

    def encode(self, x):
        nmask, nodes, emask, edges, geoms = x

        dists = ut.soft_1hot(geoms[:, :, :, -1:], *self.dist_enc, mask=emask)
        ngeom = torch.sum(geoms[:, :, :, :3] * emask.float(), dim=2) # consider mean for vects
        edges = torch.cat([edges, dists], dim=-1)

        h = [nmask, nodes, ngeom, emask, edges, geoms]
        h = self.graphconv(h)
        h = self.readout(h[1], nmask)
        mu, logvar = self.out_mu(h), self.out_logvar(h)

        # sample
        std = logvar.exp()
        err = torch.randn_like(std)
        z = mu + err * std
        self.DKL_vae = -0.5 * torch.sum(1 + 2*logvar - mu.pow(2) - logvar.exp().pow(2))

        return z

    def decode(self, z):
        # generation
        h = self.linr(z)
        nodes_d = self.decompress(h)
        nB, nA, _ = nodes_d.shape
        device = nodes_d.device
        nmask_d = torch.ones((nB, nA, 1), dtype=torch.int64, device=device)
        ngeom_d = torch.ones((nB, nA, 3), dtype=torch.float32, device=device)
        emask_d = torch.ones((nB, nA, nA, 1), dtype=torch.int64, device=device)
        edges_d = torch.ones((nB, nA, nA, 1), dtype=torch.float32, device=device)
        geoms_d = torch.ones((nB, nA, nA, 4), dtype=torch.float32, device=device)

        h = [nmask_d, nodes_d, nmask_d, emask_d, edges_d, geoms_d]
        h = self.graphtranspose(h)

        nodes_d, edges_d = h[1], h[4]

        nodes_d = self.n_out(nodes_d)
        edges_d = self.e_out((edges_d+edges_d.permute(0,2,1,3))/2)
        # think about making all the diagonal non bonds

        return nodes_d, edges_d




target_idx = [5]
latents = 100
qm9_root = os.path.expanduser('~/data/qm9')
mol_ds = md.QM9(qm9_root, padding_size = 29, target_idx = target_idx, normalization=True)
valid_len, test_len, total_len = 10000, 10000, len(mol_ds)
splits = [total_len-test_len-valid_len, test_len, valid_len]
train_dl, valid_dl, test_dl = md.mol_data_loader(
    mol_ds, splits=splits, batch_size = 128, shuffle = True, num_workers = 12)

# one hot distance
dist_enc = (12, 0.5, 2.0)

model = GVAE(node_dim=5, edge_dim=4, latents=latents, 
    dist_encoding=dist_enc).to(device)
model.load_state_dict(torch.load('vae_w.pt'))
opt = torch.optim.Adam(model.parameters(), lr=1e-4)
opt.load_state_dict(torch.load('vae_o.pt'))
mse_loss = nn.MSELoss(reduction='sum')
l1_loss = nn.L1Loss(reduction='sum')
acc = lambda y_, y: torch.abs(y_ - y).sum().detach()

def ce_loss(x_, x):
    return -torch.sum(x*x_.log())


def train(epoch):

    train_len = len(train_dl.dataset)
    desc_text = "epoch: {}, tr_loss: {:.3f}, va_loss: {:.3f}"
    tbar = tqdm(desc = desc_text.format(epoch, 0, 0), total = train_len)

    train_loss = 0.
    train_samples = 0

    model.train()
    for X, y in train_dl:
        X = [x.to(device) for x in X]
        X_ = model(X)
        nmask, nodes, emask, edges, _ = X
        nodes_d, edges_d = X_

        pad_edges = torch.cat([edges, 1-emask.float()], dim=-1)
        pad_nodes = torch.cat([nodes, 1-nmask.float()], dim=-1)

        opt.zero_grad()
        loss = ce_loss(nodes_d, pad_nodes) + ce_loss(edges_d, pad_edges) + model.DKL_vae
        loss.backward()
        opt.step()

        train_loss += loss.item() / (sum(nodes_d.shape) + sum(edges_d.shape))
        train_samples += len(y)

        tbar.set_description(desc_text.format(epoch, train_loss/train_samples, 0) )
        tbar.update(len(y))

    tbar.close()
    torch.save(model.state_dict(), 'vae_w.pt')
    torch.save(opt.state_dict(), 'vae_o.pt')

## training
# for epoch in range(100):
#     train(epoch)





## reconstruct the molecule
from modules import chem
# dataloader = train_dl
dataloader = test_dl

total_valids, total_counts = 0, 0
tbar_txt = "valid molecules: {}/{} ({:.1f}%)"

## test reconstruction
tbar = tqdm(desc = tbar_txt.format('Nan', 'Nan', 0.), total = len(dataloader.dataset))
for X, y in dataloader:
    X = [x.to(device) for x in X]
    X_ = model(X)
    nmask, nodes, emask, edges, _ = X
    pad_edges = torch.cat([edges, 1-emask.float()], dim=-1)
    pad_nodes = torch.cat([nodes, 1-nmask.float()], dim=-1)

    nodes_d, edges_d = X_

    # total_valids += chem.features_to_molecule(nodes_d, edges_d, pad_nodes, pad_edges)
    total_valids += chem.check_validity(nodes_d, edges_d)
    total_counts += nodes_d.shape[0]

    tbar.set_description(tbar_txt.format(total_valids, total_counts, 100*total_valids/total_counts))
    tbar.update(nodes_d.shape[0])

## test random sampling
# N, batch_size = 100, 128
# tbar = tqdm(desc = tbar_txt.format('Nan', 'Nan', 0.), total = N*batch_size)
# for i in range(100):
#     z = torch.randn((batch_size, latents), device=device)
#     nodes_d, edges_d = model.decode(z)
#
#     total_valids += chem.check_validity(nodes_d, edges_d)
#     total_counts += nodes_d.shape[0]
#     tbar.set_description(tbar_txt.format(total_valids, total_counts, 100*total_valids/total_counts))
#     tbar.update(nodes_d.shape[0])
# tbar.close()





