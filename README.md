Neural Message Passing on High Order Paths
==========================================

Repo for high order neural message passing. Use the example `run_*.py` scripts for the experiments.


Dataset (QM9)
-------------

The script uses `modules/moldataset.py` to load QM9. 
Using `QM9()` and `QM8()` will automatically download and modify the molecules if provided with an path.
Please see `README_qm9.md` for QM9's details (also automatically downloaded).


Environment
-----------

`conda env create -f env.yml`

