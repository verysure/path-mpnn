## faster graphconv
from torch import nn
import torch
import numpy as np
from torch.autograd import Variable


class GraphConv(nn.Module):

    def __init__(self, edge_module, message_module, combine_module, update_module):
        super(GraphConv, self).__init__()
        self.edge_module = edge_module
        self.message_module = message_module
        self.combine_module = combine_module
        self.update_module = update_module

    def forward(self, nodes, edges):
        # nodes: nBatch, nAtoms, nFeatures
        # edges: nBatch, nA, nA, nFeatures
        edges = self.edge_module( nodes, edges )
        messages = self.message_module( nodes, edges )
        combined_message = self.combine_module( messages )
        return self.update_module( nodes, combined_message ), edges



# Helper Modules
class NodeEdgeNodeLinear(nn.Module):
    "Fast Style"
    def __init__(self, node_dim, edge_dim, out_dim):
        super(NodeEdgeNodeLinear, self).__init__()
        self.dense = nn.Linear(2*node_dim+edge_dim, out_dim)
        self.activation = nn.Tanh()

    def forward(self, nodes, edges):
        # feature graph:  nodes-edge-node
        num_atoms = nodes.size()[1]
        top_nodes = nodes.unsqueeze(2).expand(-1, -1, num_atoms, -1)
        bot_nodes = nodes.unsqueeze(1).expand(-1, num_atoms, -1, -1)
        features = torch.cat((top_nodes, edges, bot_nodes), dim = 3)
        return self.activation( self.dense( features ) )

class EdgePassThrough(nn.Module):
    def __init__(self):
        super(EdgePassThrough, self).__init__()
    def forward(self, nodes, edges):
        return edges
    
class Mean(nn.Module):
    def __init__(self, dim):
        super(Mean, self).__init__()
        self.dim = dim

    def forward(self, messages):
        # messages: to N, nA, nA, features
        return torch.mean( messages, dim=self.dim )

class Attention(nn.Module):
    def __init__(self, in_dim, dim):
        super(Attention, self).__init__()
        self.projection = nn.Linear(in_dim, 1, bias = False)
        self.dim = dim

    def forward(self, sets):
        e = self.projection(sets)
        a = nn.Softmax(dim=self.dim)(e)
        print(e[0, :5, :5, 0])
        exit()
        return torch.sum( a * sets, dim=self.dim )


class Set2Set(nn.Module):

    def __init__(self, in_dim, out_dim, dim, steps = 2):
        super(Set2Set, self).__init__()
        assert out_dim > in_dim
        self.total_dim = out_dim
        self.lstm = nn.Sequential(
            nn.Linear(self.total_dim, out_dim-in_dim),
            nn.Tanh(), )
        self.projection = nn.Linear(self.total_dim, 1, bias = False)
        self.dim = dim
        self.steps = steps

    def forward(self, sets):
        set_sizes = sets.size()
        num_atoms = sets.size()[1]
        qs = Variable(sets.data.new(set_sizes[0], self.total_dim).zero_())

        for step in range(self.steps):
            q = self.lstm(qs)
            expand_q = q.unsqueeze(1).expand(-1, num_atoms, -1)
            
            e = self.projection(torch.cat([sets, expand_q], dim=-1))
            a = nn.Softmax(dim=self.dim)(e)
            r = torch.sum( a * sets, dim=self.dim )
            qs = torch.cat([q, r], dim=-1)
        return qs




# Update Module
class NNUpdate(nn.Module):
    def __init__(self, node_dim, message_dim, out_dim):
        super(NNUpdate, self).__init__()
        self.dense = nn.Linear(node_dim + message_dim, out_dim)
        self.activation = nn.Tanh()

    def forward(self, nodes, messages):
        features = torch.cat([nodes, messages], dim=2)
        return self.activation( self.dense( features ) )



