## molecular dataset
import torch
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.dataset import random_split, Subset
import torchvision.transforms as transforms
from torch.nn.functional import pad
# from rdkit import Chem
from tqdm import tqdm
import numpy as np
import pandas as pd

import glob, os, re, wget
import tarfile


atom_types = ['H', 'C', 'O', 'N', 'F']
# hybridization_types = [
#     Chem.rdchem.HybridizationType.UNSPECIFIED, 
#     Chem.rdchem.HybridizationType.SP, 
#     Chem.rdchem.HybridizationType.SP2, 
#     Chem.rdchem.HybridizationType.SP3,
#     ]
bond_types = [1,2,3,4]

# one hot functions
def one_hot(idx, max_idx):
    return list( 1*(idx == np.arange(max_idx)) )
def one_hot_types(type_name, type_list):
    return list( 1*(type_name == np.array(type_list)) )
def one_hot_values(value, length, max_value, min_value = 0.):
    values = [0.]*length
    idx = int((value-min_value) / (max_value - min_value) * (length-1))
    idx = 0 if idx < 0 else idx
    idx = length-1 if idx > length-1 else idx
    values[idx] = 1.
    return values

# single file
def parse_mol_text(mol_text, dist_encoding = None):

    ## datastructure (+1 is for the first index to be a dummy variable)
    # nodes: nA+1 x nF1
    # edges: nE+1 x nF2
    # neig1: nA+1 x 4 x 1
    # neig2: nA+1 x 12 x 2
    # neig3: nA+1 x 36 x 3
    # tree structure??
    # neig: 
    # geom1:

    # parse mol_text
    mol_lines = mol_text.split('\n')
    counts = mol_lines[3].split()
    num_bonds = int(counts[1])

    num_atoms = int(counts[0])
    dist_length   = 0 if dist_encoding is None else dist_encoding[0]

    # nodes = torch.tensor
    nmask = np.ones((num_atoms+1, 1), dtype=int)
    nodes = np.zeros((num_atoms+1, len(atom_types)))
    emask = np.ones((num_bonds+1, 1), dtype = int)
    edges = np.zeros((num_bonds+1, len(bond_types)+dist_length))
    edges_neig = np.zeros((num_bonds+1, 2), dtype=int)
    xyz = np.zeros((num_atoms+1, 3))

    nmask[0, 0] = 0
    emask[0, 0] = 0

    for i in range(num_atoms):
        x, y, z, atom = mol_lines[4+i].split()[:4]
        xyz[i+1, :] = [float(x), float(y), float(z)]

        atom_1hot = one_hot_types(atom, atom_types)
        nodes[i+1, :] = atom_1hot

    graph_path = {}
    graph_edges = {}

    def add_neigbor(graph_path, ai, aj):
        # add neigbors
        if graph_path.get(ai, None) is None:
            graph_path[ai] = []
        graph_path[ai].append(aj)
        if graph_path.get(aj, None) is None:
            graph_path[aj] = []
        graph_path[aj].append(ai)

    def add_edges(graph_edges, ai, bi):
        if graph_edges.get(ai, None) is None:
            graph_edges[ai] = []
        graph_edges[ai].append(bi)
    
    # get bonds
    for bi, bond_line in enumerate(mol_lines[4+num_atoms:4+num_atoms+num_bonds]):
        ai, aj, bond_type = list(map(int, bond_line.split()[:3]))

        bond_1hot = one_hot_types(bond_type, bond_types)
        if dist_encoding is None:
            dist_1hot = []
        else:
            dist_1hot = one_hot_values(length, dist_encoding[0], 
                min_value = dist_encoding[1], 
                max_value = dist_encoding[2])

        add_neigbor(graph_path, ai, aj)
        add_edges(graph_edges, ai, bi+1)
        add_edges(graph_edges, aj, bi+1)
        edges_neig[bi+1, :] = [ai, aj]
        edges[bi+1, :] = bond_1hot + dist_1hot


    ## 1st order paths
    neig1 = np.zeros((num_atoms+1, 4), dtype=int)
    for k, n in graph_path.items():
        neig1[k][:len(n)] = n

    edge1 = np.zeros((num_atoms+1, 4), dtype=int)
    for k, n in graph_edges.items():
        edge1[k][:len(n)] = n


    ## 2nd order paths
    neig2 = np.take(neig1, neig1, axis=0).reshape((num_atoms+1, -1))
    neig2 = np.stack([neig1.repeat(4, 1), neig2], axis=-1)
    edge2 = np.take(edge1, neig1, axis=0).reshape((num_atoms+1, -1))
    edge2 = np.stack([edge1.repeat(4, 1), edge2], axis=-1)

    # remove incorrect paths
    selfidx = np.arange(num_atoms+1, dtype=int)[:, None]
    zero_mask = np.all(neig2 != 0, axis=-1)
    self_mask = neig2[:, :, -1] != selfidx
    path_mask = zero_mask*self_mask
    neig2 = path_mask[:, :, None]*neig2
    edge2 = path_mask[:, :, None]*edge2

    # sort and take first 12
    sorted_idx = neig2[:, :, 0].argsort(axis=1)[:, ::-1, None]
    neig2 = np.take_along_axis(neig2, sorted_idx, axis=1)
    neig2 = neig2[:, :12, :]
    edge2 = np.take_along_axis(edge2, sorted_idx, axis=1)
    edge2 = edge2[:, :12, :]

    ## 3rd order paths
    neig3 = np.take(neig1, neig2[:, :, -1], axis=0)
    neig3 = neig3.reshape((num_atoms+1, -1, 1))
    neig3 = np.concatenate([neig2.repeat(4, 1), neig3], axis=-1)
    edge3 = np.take(edge1, neig2[:, :, -1], axis=0)
    edge3 = edge3.reshape((num_atoms+1, -1, 1))
    edge3 = np.concatenate([edge2.repeat(4, 1), edge3], axis=-1)

    # remove incorrect paths
    selfidx = np.arange(num_atoms+1, dtype=int)[:, None]
    zero_mask = np.all(neig3 != 0, axis=-1)
    self_mask = neig3[:, :, -1] != selfidx
    prev_mask = neig3[:, :, -1] != neig3[:, :, 0]
    path_mask = zero_mask*self_mask*prev_mask
    neig3 = path_mask[:, :, None]*neig3
    edge3 = path_mask[:, :, None]*edge3

    # sort and take first 36
    sorted_idx = neig3[:, :, 0].argsort(axis=1)[:, ::-1, None]
    neig3 = np.take_along_axis(neig3, sorted_idx, axis=1)
    neig3 = neig3[:, :36, :]
    edge3 = np.take_along_axis(edge3, sorted_idx, axis=1)
    edge3 = edge3[:, :36, :]

    ## construct geom
    norm = lambda d, ax=-1: np.sum(d**2, axis=ax) ** 0.5

    geom1 = np.take(xyz, neig1, axis=0) - xyz[:, None, :]
    geom1 = norm(geom1) * (neig1 > 0)

    vect2 = np.take(xyz, neig2, axis=0)
    vect2[:, :, 1, :] -= vect2[:, :, 0, :]
    vect2[:, :, 0, :] -= xyz[:, None, :]
    geom2 = norm(vect2) * (neig2 > 0)
    vdot2 = np.sum(vect2[:, :, 1:, :] * vect2[:, :, :-1, :], axis=-1)
    dot_norms = np.prod(geom2, axis=-1)
    dot_norms[dot_norms<1e-9] += 1e-9
    vdot2 /= dot_norms[:, :, None]
    geom2 = np.concatenate([geom2, vdot2], axis=-1)

    vect3 = np.take(xyz, neig3, axis=0)
    vect3[:, :, 1:, :] -= vect3[:, :, :-1, :]
    vect3[:, :, 0, :] -= xyz[:, None, :]

    geom3 = norm(vect3) * (neig3 > 0)
    vdot3 = np.sum(vect3[:, :, 1:, :] * vect3[:, :, :-1, :], axis=-1)
    dot_norms = geom3[:, :, 1:] * geom3[:, :, :-1]
    dot_norms[dot_norms<1e-9] += 1e-9
    vdot3 /= dot_norms

    vout3 = np.cross(vect3[:, :, 1:, :], vect3[:, :, :-1, :], axis=-1)
    vdih3 = np.sum(vout3[:, :, 1:, :] * vout3[:, :, :-1, :], axis=-1)
    dot_norms = np.prod(norm(vout3, -1), axis=-1)[:, :, None]
    dot_norms[dot_norms<1e-9] += 1e-9

    geom3 = np.concatenate([geom3, vdot3, vdih3], axis=-1)

    ## auto checking path validity
    # edge_dict = {}
    # for i in range(num_bonds+1):
    #     ai, aj = edges_neig[i, :]
    #     edge_dict[(ai, aj)] = i
    #     edge_dict[(aj, ai)] = i
    # for _ in range(5):
    #     empty = True
    #     while empty:
    #         aid = np.random.randint(1, num_atoms+1, 1)[0]
    #         pid = np.random.randint(0, 36, 1)[0]
    #         if neig3[aid, pid, 0] != 0:
    #             # check the edges are correct to the path
    #             nlist = [aid] + list(neig3[aid, pid, :])
    #             for i in range(3):
    #                 node_e = edge_dict[(nlist[i], nlist[i+1])]
    #                 assert edge3[aid, pid, i] == node_e
    #             # print([aid] + list(neig3[aid, pid, :]), edge3[aid, pid, :])
    #             empty = False


    neig1, edge1, geom1 = [neig1[:, :, None], edge1[:, :, None], geom1[:, :, None]]

    return nmask, nodes, emask, edges, neig1, edge1, geom1, neig2, edge2, geom2, neig3, edge3, geom3, edges_neig


def parse_qm9_blacklist(fname):
    with open(fname, 'r') as f:
        lines = f.readlines()
    lines = lines[9:3063]
    return [line.split()[0] for line in lines]

def tensor_pad(array, shape):
    tensor = torch.tensor(array)
    tensor = pad(tensor, shape, 'constant', 0)
    return tensor


def download_and_extract(url, folder):
    if not os.path.isdir(folder):
        os.makedirs(folder)
        wget.download(url, out = folder)
        print('')
        fname = os.path.join(folder, url.split('/')[-1])
        with tarfile.open(fname) as tar:
            for t in tqdm(tar):
                tar.extract(t, path=folder)
        os.remove(fname)

# parent class, need to be extended to use
class GDB9(Dataset):
    def __init__(self, root, dist_encoding = None, node_padding=30, 
        edge_padding=29, target_idx = None, normalization=False):

        download_dataset(root)
        self.dist_encoding = dist_encoding
        self.node_padding = node_padding
        self.edge_padding = edge_padding
        self.target_idx = target_idx
        self.normalization = normalization

        # parse sdf
        with open(os.path.join(root, 'gdb9', 'gdb9.sdf'), 'r') as f:
            self.mol_texts = f.read().split('$$$$\n')[:-1]

        # Need override!
        self.props, self.mean, self.std, self.whitelist = None, None, None, None

    def __len__(self):
        return len(self.whitelist)

    def __getitem__(self, idx):
        mol_id = self.whitelist[idx]
        mol_text = self.mol_texts[mol_id]

        # nmask, nodes, emask, edges, neig1, edge1, geom1, neig2, edge2, geom2, neig3, edge3, geom3, edges_neig
        X = list(parse_mol_text(mol_text, self.dist_encoding))

        dn = self.node_padding - X[1].shape[0]
        assert dn >= 0

        de = self.edge_padding - X[3].shape[0]
        assert de >= 0

        for i in [0, 1]:
            X[i] = tensor_pad(X[i], (0,0,0,dn))
        for i in [2, 3, 13]:
            X[i] = tensor_pad(X[i], (0,0,0,de))
        for i in range(4, 13):
            X[i] = tensor_pad(X[i], (0,0,0,0,0,dn))

        # floats
        for i in [1, 3, 6, 9, 12]:
            X[i] = X[i].float()

        props = self.props[idx]
        if self.normalization:
            props = (props-self.mean)/self.std
        if self.target_idx is not None:
            props = props[self.target_idx]
        props = torch.tensor(props).float()

        return X, props



class QM9(GDB9):
    def __init__(self, root, dist_encoding = None, node_padding=30, 
        edge_padding=29, target_idx = None, normalization=False):
        super().__init__(root, dist_encoding, node_padding, 
            edge_padding, target_idx, normalization)

        # normalization
        with open(os.path.join(root, 'gdb9', 'gdb9_norms.csv'), 'r') as f:
            self.mean = np.array([float(s) for s in f.readline().split(',')])
            self.std = np.array([float(s) for s in f.readline().split(',')])

        # get whitelist
        with open(os.path.join(root, 'blacklist.txt'), 'r') as f:
            blacklist = [int(b)-1 for b in f.readline().split(',')]
        mask = np.ones(len(self.mol_texts), dtype=bool)
        mask[blacklist] = False
        self.whitelist = np.arange(len(self.mol_texts), dtype=int)[mask]

        # props
        self.props = pd.read_csv(os.path.join(root, 'gdb9', 'gdb9.sdf.csv'), 
            usecols = [
                'A','B','C','mu','alpha','homo','lumo',
                'gap','r2','zpve','u0','u298','h298','g298','cv'],
            index_col=False).to_numpy()
        self.props = self.props[self.whitelist, :]



class QM8(GDB9):
    def __init__(self, root, dist_encoding = None, node_padding=30, 
        edge_padding=29, target_idx = None, normalization=False):
        super().__init__(root, dist_encoding, node_padding, 
            edge_padding, target_idx, normalization)

        # normalization
        with open(os.path.join(root, 'qm8_norms.txt'), 'r') as f:
            self.mean = np.array([float(s) for s in f.readline().split(',')])
            self.std = np.array([float(s) for s in f.readline().split(',')])

        # get whitelist
        self.whitelist = np.loadtxt(os.path.join(root, 'qm8.txt'))[:, 0].astype(int)-1

        # props
        self.props = np.loadtxt(os.path.join(root, 'qm8.txt'))[:, 1:]


def download_dataset(root):
    if not os.path.isdir(root):
        os.makedirs(root)

    # remove dataset
    if not os.path.isfile(os.path.join(root, 'uncharacterized.txt')):
        wget.download(
            'https://s3-eu-west-1.amazonaws.com/pstorage-npg-968563215/3195404/uncharacterized.txt',
            out = root, )
        print('')
        blacklist = parse_qm9_blacklist(os.path.join(root, 'uncharacterized.txt'))
        with open(os.path.join(root, 'blacklist.txt'), 'w') as f:
            f.write(','.join(blacklist))

    # download the gdb9 dataset
    with open(os.path.join(root, 'blacklist.txt'), 'r') as f:
        blacklist = f.readline().split(',')
    download_and_extract(
        'http://deepchem.io.s3-website-us-west-1.amazonaws.com/datasets/gdb9.tar.gz',
        os.path.join(root, 'gdb9') )
    # calculate qm9 normalization
    sdf_file = os.path.join(root, 'gdb9', 'gdb9.sdf.csv')
    norms_file = os.path.join(root, 'gdb9', 'gdb9_norms.csv')
    if not os.path.isfile(norms_file):
        df = pd.read_csv(sdf_file)
        df = df[[
            'A','B','C','mu','alpha','homo','lumo',
            'gap','r2','zpve','u0','u298','h298','g298','cv'
        ]]
        if blacklist is not None:
            blacklist = [int(b)-1 for b in blacklist]
            df = df.drop(blacklist)
        std = df.std(axis=0)
        mean = df.mean(axis=0)
        with open(norms_file, 'w') as f:
            f.write(','.join([str(m) for m in mean]))
            f.write('\n')
            f.write(','.join([str(s) for s in std]))

    # download the qm8 dataset
    qm8_file = os.path.join(root, 'qm8.txt')
    qm8_norm_file = os.path.join(root, 'qm8_norms.txt')

    if not os.path.isfile(qm8_file):
        wget.download(
            'ftp://ftp.aip.org/epaps/journ_chem_phys/E-JCPSA6-143-043532/gdb8_22k_elec_spec.txt',
            out = qm8_file, )
        print('')
        
    if not os.path.isfile(qm8_norm_file):
        data = np.loadtxt(qm8_file)[:, 1:]
        mean = np.mean(data, axis=0)
        std = np.std(data, axis=0)
        with open(qm8_norm_file, 'w') as f:
            f.write(','.join([str(m) for m in mean]))
            f.write('\n')
            f.write(','.join([str(s) for s in std]))



def mol_data_loader(dataset, splits=[], idx=[], **kwargs):
    if len(splits) > 0:
        datasets = random_split(dataset, splits)
        return [DataLoader(ds, **kwargs) for ds in datasets]
    elif len(idx) > 0:
        return DataLoader(Subset(dataset, idx), **kwargs)
    else:
        return DataLoader(dataset, **kwargs)
    return



if __name__ == '__main__':
    from pprint import pprint
    from rdkit import Chem
    import pybel
    folder = os.path.expanduser('~/data/qm9')

    # test newly generated data
    # fname = os.path.expanduser('~/data/qm9/gdb9/gdb9.sdf')
    # with open(fname, 'r') as f:
    #     mol_texts = f.read().split('$$$$\n')[:-1]
    # data = []
    # for mol_text in mol_texts:
    #     nums = mol_text.split('\n')[3].split()[:2]
    #     data.append([int(nums[0]), int(nums[1])])
    # print(np.max(data, axis=0))
    # print(len(mol_texts))
    # exit()

    # parse data
    # for i in range(1, 140000):
    #     fname = os.path.join(folder, 'dsgdb9nsd', 'dsgdb9nsd_{:06d}.xyz'.format(i))
    #     if not os.path.isfile(fname):
    #         fname = os.path.join(folder, 'uncharacterized', 'dsgdb9nsd_{:06d}.xyz'.format(i))
    #     # still no file
    #     if not os.path.isfile(fname):
    #         continue
    #
    #
    #     with open(fname, 'r') as f:
    #         data = f.read()
    #     mol = pybel.readstring('xyz', data)
    #     with open('./gdb9.sdf2', 'a') as f:
    #         f.write(mol.write('sdf'))
    #
    #     if i % 1000 == 0:
    #         print(i)
    # exit()



    folder = os.path.expanduser('~/data/qm9')
    mol_ds = QM8(folder, target_idx=[5, 6, 7])

    fname = os.path.expanduser('~/data/qm9/gdb9/gdb9.sdf')
    with open(fname, 'r') as f:
        mol_texts = f.read().split('$$$$\n')[:-1]
    data = []
    for idx in mol_ds.whitelist:
        mol_text = mol_texts[idx]
        nums = mol_text.split('\n')[3].split()[:2]
        data.append([int(nums[0]), int(nums[1])])
    print(np.max(data, axis=0))
    print(len(mol_texts))
    exit()

    mol_ds[10000]
    # import time
    # st = time.time()
    # for i, (X, y) in enumerate(mol_ds):
    #     if i % 5000 == 0:
    #         print(i)
    # print(time.time()-st)
    # exit()

    mol_dl = torch.utils.data.DataLoader(
        mol_ds, batch_size = 64, shuffle = True, num_workers = 4)

    for (M, N, C, E, G1, G2, G3, NG), y in mol_dl:
        print(M.shape, N.shape, C.shape, E.shape, NG.shape, y.shape)
        print(M.dtype, N.dtype, C.dtype, E.dtype, NG.dtype, y.dtype)

        print(G1[0].shape, G2[0].shape, G3[0].shape)
        print(G1[0].dtype, G2[0].dtype, G3[0].dtype)
        break
    pass


