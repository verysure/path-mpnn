import torch
from rdkit import Chem
import numpy as np
from rdkit import rdBase
rdBase.DisableLog('rdApp.*')

atom_types = np.array(['H', 'C', 'O', 'N', 'F'])
def features_to_molecule(nodes, edges):
    atoms = nodes.argmax(dim=-1)
    bonds = edges.argmax(dim=-1)

    # atoms_t = nodes_t.argmax(dim=-1)
    # bonds_t = edges_t.argmax(dim=-1)

    noatom_idx = nodes.shape[-1]-1
    nobond_idx = edges.shape[-1]-1
    num_atoms = torch.sum(atoms != noatom_idx, dim=-1)
    num_bonds = (bonds != nobond_idx).sum(dim=-1).sum(dim=-1)

    counts = 0
    idx = 0
    for idx in range(nodes.shape[0]):
        # generate molblocks
        has_atoms = (atoms[idx] != noatom_idx).long()
        has_bonds = (bonds[idx] != nobond_idx).sum(dim=-1)
        atom_idx = (has_atoms * has_bonds).nonzero().cpu()[:, 0]

        count_fmt = '{:>3d}{:>3d}  0  0  0  0  0  0  0  0999 V2000'
        atom_fmt = '{:>10.4f}{:>10.4f}{:>10.4f}{:>2s}   0  0  0  0  0  0  0  0  0  0  0  0'
        bond_fmt = '{:>3d}{:>3d}{:>3d}  0'
        atom_blocks = []
        bond_blocks = []
        for i, i_idx in enumerate(atom_idx):
            atom_blocks.append(atom_fmt.format(0, 0, 0, atom_types[atoms[idx, i_idx]]))

            for j, j_idx in enumerate(atom_idx):
                bond = bonds[idx, i_idx, j_idx]
                if j > i and bond != nobond_idx:
                    bond_blocks.append(bond_fmt.format(i+1, j+1, bond+1))


        molblocks = ['', 'gvae', '', count_fmt.format(len(atom_blocks), len(bond_blocks))] + atom_blocks + bond_blocks + ['M  END\n']
        mol = Chem.MolFromMolBlock('\n'.join(molblocks))
        if mol is not None:
            counts += 1
        # print(Chem.MolToSmiles(mol))

    return counts
    # TODO:
    # return the molecules or accuracies



def check_validity(nodes, edges):
    atoms = nodes.argmax(dim=-1)
    bonds = edges.argmax(dim=-1)

    noatom_idx = nodes.shape[-1]-1
    nobond_idx = edges.shape[-1]-1
    num_atoms = torch.sum(atoms != noatom_idx, dim=-1)
    num_bonds = (bonds != nobond_idx).sum(dim=-1).sum(dim=-1)

    counts = 0
    idx = 0
    for idx in range(nodes.shape[0]):
        # generate molblocks
        has_atoms = (atoms[idx] != noatom_idx).long()
        has_bonds = (bonds[idx] != nobond_idx).sum(dim=-1)
        atom_idx = (has_atoms * has_bonds).nonzero().cpu()[:, 0]

        count_fmt = '{:>3d}{:>3d}  0  0  0  0  0  0  0  0999 V2000'
        atom_fmt = '{:>10.4f}{:>10.4f}{:>10.4f}{:>2s}   0  0  0  0  0  0  0  0  0  0  0  0'
        bond_fmt = '{:>3d}{:>3d}{:>3d}  0'
        atom_blocks = []
        bond_blocks = []
        for i, i_idx in enumerate(atom_idx):
            atom_blocks.append(atom_fmt.format(0, 0, 0, atom_types[atoms[idx, i_idx]]))

            for j, j_idx in enumerate(atom_idx):
                bond = bonds[idx, i_idx, j_idx]
                if j > i and bond != nobond_idx:
                    bond_blocks.append(bond_fmt.format(i+1, j+1, bond+1))


        molblocks = ['', 'gvae', '', count_fmt.format(len(atom_blocks), len(bond_blocks))] + atom_blocks + bond_blocks + ['M  END\n']
        mol = Chem.MolFromMolBlock('\n'.join(molblocks))
        if mol is not None:
            counts += 1
        # print(Chem.MolToSmiles(mol))

    return counts
    # TODO:
    # return the molecules or accuracies









