# 3d graph conv
from torch import nn
import torch
import numpy as np
from torch.autograd import Variable
import utils as ut

def adjacency_to_index(adjacency):

    # print(adjacency.shape)
    adjacency = adjacency[0, :, :, 0]

    # print(adjacency)

    print(adjacency.nonzero())


    return

class GraphConv3D(nn.Module):
    def __init__(self, edge_module, message_module, combine_module, update_module, geometry_module = None):
        super().__init__()
        self.edge_module = edge_module
        self.message_module = message_module
        self.combine_module = combine_module
        self.update_module = update_module
        self.geometry_module = geometry_module

    def forward(self, x):
        # nmask: nBatch, nA, 1 (mask for used atoms)
        # nodes: nBatch, nAtoms, nFeatures
        # edges: nBatch, nEdges, nFeatures
        # ngeom: nBatch, nA, 3 (xyz)

        # neig1
        # neig2
        # neig3

        # emask: nBatch, nA, nA, 1 (mask for used edges)
        # edges: nBatch, nA, nA, nFeatures
        # geoms: nBatch, nA, nA, 3+1 (3 normalized xyz and length)

        nmask, nodes, ngeom, emask, edges, geoms = x

        edges = self.edge_module(  nodes, ngeom, edges, geoms  )
        messages = self.message_module( nodes, ngeom, edges, geoms )
        combined_message = self.combine_module( messages, emask, nmask )
        nodes = self.update_module( nodes, combined_message )

        if self.geometry_module is not None:
            ngeom, geoms = self.geometry_module( messages, nmask, ngeom, emask, geoms )

        return nmask, nodes, ngeom, emask, edges, geoms


# Helper Modules
class NodeEdgeNode(nn.Module):
    "Fast Style"
    def __init__(self, node_dim, edge_dim, out_dim):
        super().__init__()
        self.linear = nn.Sequential(
            nn.Linear(2*node_dim+edge_dim, out_dim),
            nn.Tanh()
        )

    def forward(self, nodes, ngeom, edges, geoms):
        # feature graph:  nodes-edge-node
        num_atoms = nodes.size()[1]
        top_nodes = nodes.unsqueeze(2).expand(-1, -1, num_atoms, -1)
        bot_nodes = nodes.unsqueeze(1).expand(-1, num_atoms, -1, -1)
        features = torch.cat((top_nodes, edges, bot_nodes), dim = 3)
        return self.linear( features )

class NodeEdgeNode3D(nn.Module):
    "Fast Style"
    def __init__(self, node_dim, edge_dim, out_dim, angle_bins=16):
        super().__init__()
        self.linear = nn.Sequential(
            nn.Linear(2*node_dim+edge_dim+2*angle_bins, out_dim),
            nn.Tanh()
        )
        self.angle_bins = angle_bins

    def forward(self, nodes, ngeom, edges, geoms):
        # feature graph:  node-node-geom-theta-alpha
        nA = nodes.size()[1]

        tensors = []
        tensors.append(nodes.unsqueeze(2).expand(-1, -1, nA, -1))
        tensors.append(nodes.unsqueeze(1).expand(-1, nA, -1, -1))
        tensors.append(edges)

        nvect = ngeom.unsqueeze(2).expand(-1, -1, nA, -1)
        theta = torch.sum(nvect * geoms[:, :, :, :3], dim=-1)
        t_1hot = ut.soft_1hot(theta.unsqueeze(-1), self.angle_bins, -1, 1)
        tensors.append(t_1hot)
        # tensors.append(theta.unsqueeze(-1))

        torque = torch.cross(nvect, geoms[:, :, :, :3], dim=-1)
        alpha = torch.sum(torque * torque.transpose(1, 2), dim=-1)
        a_1hot = ut.soft_1hot(alpha.unsqueeze(-1), self.angle_bins, -1, 1)
        tensors.append(a_1hot)
        # tensors.append(alpha.unsqueeze(-1))

        # need checking whether the cross and dot is doing what we want
        features = torch.cat(tensors, dim = 3)
        return self.linear( features )

# maybe need a new edge model
class EdgeMatrixMessages(nn.Module): # mpnn style
    "Fast Style"
    def __init__(self, node_dim, edge_dim, out_dim, angle_bins=16):
        super().__init__()
        self.linear = nn.Sequential(
            nn.Linear(edge_dim+2*angle_bins, out_dim*node_dim),
            nn.Tanh()
        )
        self.angle_bins = angle_bins
        self.node_dim = node_dim

    def forward(self, nodes, ngeom, edges, geoms):
        # feature graph:  node-node-geom-theta-alpha
        nA = nodes.size()[1]

        tensors = []

        tensors.append(edges)
        nvect = ngeom.unsqueeze(2).expand(-1, -1, nA, -1)

        theta = torch.sum(nvect * geoms[:, :, :, :3], dim=-1)
        t_1hot = ut.soft_1hot(theta.unsqueeze(-1), self.angle_bins, -1, 1)
        tensors.append(t_1hot)

        torque = torch.cross(nvect, geoms[:, :, :, :3], dim=-1)
        alpha = torch.sum(torque * torque.transpose(1, 2), dim=-1)
        a_1hot = ut.soft_1hot(alpha.unsqueeze(-1), self.angle_bins, -1, 1)
        tensors.append(a_1hot)

        edge_features = torch.cat(tensors, dim = 3)

        # multiplication
        em_sizes = edge_features.shape[:3]
        edge_matrix = self.linear(edge_features).view(*em_sizes, -1, self.node_dim) / self.node_dim
        neig_nodes = nodes.unsqueeze(1).expand(-1, nA, -1, -1).unsqueeze(-2)
        messages = torch.sum(edge_matrix*neig_nodes, dim=-1)
        return messages


class GraphDropout(nn.Module):
    def __init__(self, p):
        super().__init__()
        self.dropout = nn.Dropout(p=p)
    def forward(self, x):
        nmask, nodes, ngeom, emask, edges, geoms = x
        nodes = self.dropout(nodes)
        edges = self.dropout(edges)
        return nmask, nodes, ngeom, emask, edges, geoms

class EdgePassThrough(nn.Module):
    def __init__(self):
        super(EdgePassThrough, self).__init__()
    def forward(self, nodes, ngeom, edges, geoms):
        return edges
    
## geomtetry modules ##
class NodeDirection(nn.Module):
    def __init__(self, message_dim):
        super().__init__()
        self.projection = nn.Linear(message_dim, 1, bias = False)

    def forward(self, messages, nmask, ngeom, emask, geoms):
        e = self.projection(messages)
        e += (emask-1).float()*1e10
        a = nn.Softmax(dim=2)(e)

        ngeom = torch.sum(geoms[:, :, :, :3] * emask.float() * a, dim=2)
        return ngeom, geoms


## combination modules ##
class Mean(nn.Module):
    def __init__(self, dim):
        super(Mean, self).__init__()
        self.dim = dim

    def forward(self, messages, emask, nmask):
        # messages: to N, nA, nA, features
        agg = torch.sum( emask.float() * messages, dim=self.dim )
        num = torch.sum(emask, dim=self.dim).float() + (1-nmask.float())
        mean = nmask.float()*(agg/num)
        return mean

class Attention(nn.Module):
    def __init__(self, in_dim, dim, mean=False):
        super().__init__()
        self.projection = nn.Linear(in_dim, 1, bias = False)
        self.dim = dim
        self.mean = mean

    def forward(self, sets, mask, nmask):
        e = self.projection(sets)
        e += (mask-1).float()*1e10
        a = nn.Softmax(dim=self.dim)(e)

        if self.mean:
            agg = torch.sum( a * sets, dim=self.dim )
            num = torch.sum(mask, dim=self.dim).float() + (1-nmask.float())
            mean = nmask.float()*(agg/num)
            return mean
        else:
            return torch.sum( a * sets, dim=self.dim )

class Set2Vec(nn.Module):
    def __init__(self, in_dim, out_dim, dim, steps = 2):
        super().__init__()
        assert out_dim > in_dim
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.lstm = nn.LSTM(self.out_dim, self.out_dim-in_dim, 1)
        self.projection = nn.Linear(self.out_dim, 1, bias = False)
        self.dim = dim
        self.steps = steps

    def forward(self, sets, nmask):
        set_sizes = sets.size()
        num_atoms = sets.size()[1]
        qs = torch.zeros((set_sizes[0], 1, self.out_dim), device=sets.device)
        # TODO: double check if this is correct
        hidden = (
            torch.zeros((1, 1, self.out_dim-self.in_dim), device=sets.device), 
            torch.zeros((1, 1, self.out_dim-self.in_dim), device=sets.device), 
        )

        for step in range(self.steps):
            q, hidden = self.lstm(qs, hidden)
            expand_q = q.expand(-1, num_atoms, -1)
            
            e = self.projection(torch.cat([sets, expand_q], dim=-1))
            e += (nmask-1).float()*1e10
            a = nn.Softmax(dim=self.dim)(e)
            r = torch.sum( a * sets, dim=self.dim )
            qs = torch.cat([q, r.unsqueeze(1)], dim=-1)

        qs = qs.squeeze(1)
        return qs


class Vec2Set(nn.Module):
    def __init__(self, in_dim, out_dim, expand=29):
        super().__init__()
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.rnn = nn.RNN(self.in_dim, self.out_dim, 1, nonlinearity='tanh')
        self.expand = expand

    def forward(self, x_input):
        h = torch.zeros((1, x_input.shape[0], self.out_dim), device=x_input.device)

        x = x_input.unsqueeze(0).expand(self.expand, -1, -1)
        nodes, h = self.rnn(x, h)
        nodes = nodes.permute(1, 0, 2).contiguous()

        return nodes




## Update Modules ##
class NNUpdate(nn.Module):
    def __init__(self, node_dim, message_dim, out_dim):
        super(NNUpdate, self).__init__()
        self.dense = nn.Linear(node_dim + message_dim, out_dim)
        self.activation = nn.Tanh()

    def forward(self, nodes, messages):
        features = torch.cat([nodes, messages], dim=2)
        return self.activation( self.dense( features ) )

class GRUUpdate(nn.Module):
    def __init__(self, node_dim, hidden_dim):
        super().__init__()
        self.gru = nn.GRUCell(node_dim, hidden_dim)

    def forward(self, nodes, messages):
        nB, nA, _ = nodes.shape
        out = self.gru(nodes.view(nB*nA, -1), messages.view(nB*nA, -1))
        return out.view(nB, nA, -1)

# class GRUUpdate2(nn.Module):
#     def __init__(self, node_dim, hidden_dim):
#         super().__init__()
#         self.gru = nn.GRUCell(node_dim, hidden_dim)
#
#     def forward(self, nodes, messages):
#         nB, nA, _ = nodes.shape
#         out = self.gru(nodes.view(nB*nA, -1), messages.view(nB*nA, -1))
#         return out.view(nB, nA, -1)


## obsolete
# class Set2Set(nn.Module):
#
#     def __init__(self, in_dim, out_dim, dim, steps = 2):
#         super(Set2Set, self).__init__()
#         assert out_dim > in_dim
#         self.out = out_dim
#         # self.lstm = nn.Sequential(
#         #     nn.Linear(self.total_dim, out_dim-in_dim),
#         #     nn.Tanh(), )
#         self.lstm = nn.LSTM(self.total_dim+in_dim, self.total_dim, 1)
#         self.projection = nn.Linear(self.total_dim, 1, bias = False)
#         self.dim = dim
#         self.steps = steps
#
#     def forward(self, sets):
#         set_sizes = sets.size()
#         num_atoms = sets.size()[1]
#         qs = Variable(sets.data.new(set_sizes[0], self.total_dim).zero_())
#
#         hidden = (
#             torch.zeros((set_sizes[0], self.to), 
#         )
#
#         rnn = nn.LSTM(10, 20, 2)
#         inp = torch.randn(5, 3, 10)
#         h0 = torch.randn(2, 3, 20)
#         c0 = torch.randn(2, 3, 20)
#         output, (hn, cn) = rnn(inp, (h0, c0))
#
#         print(output.shape)
#
#         exit()
#
#         for step in range(self.steps):
#             q = self.lstm(qs)
#             expand_q = q.unsqueeze(1).expand(-1, num_atoms, -1)
#             
#             e = self.projection(torch.cat([sets, expand_q], dim=-1))
#             a = nn.Softmax(dim=self.dim)(e)
#             r = torch.sum( a * sets, dim=self.dim )
#             qs = torch.cat([q, r], dim=-1)
#
#         exit()
#         return qs


# test gather
# nodes = torch.randn((128, 29, 5))
# idx = torch.tensor([9,1,4]).unsqueeze(0).unsqueeze(2).expand(128, -1, 5)
# print(nodes.shape, idx.shape)
#
# a = torch.gather(nodes, 1, idx)
# print(a.shape)

if __name__ == '__main__':
    import moldataset as md
    import os

    folder = os.path.expanduser('~/data/qm9')
    mol_ds = md.QM9(folder, target_idx=[5, 6, 7])

    mol_dl = torch.utils.data.DataLoader(
        mol_ds, batch_size = 64, shuffle = True, num_workers = 4)

    for (M, N, C, E, G), y in mol_dl:
        adjacency_to_index(C)
        # print(M.shape, N.shape, C.shape, E.shape, G.shape, y.shape)
        # print(M.dtype, N.dtype, C.dtype, E.dtype, G.dtype, y.dtype)
        break
    pass








