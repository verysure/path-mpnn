# 3d graph conv
from torch import nn
import torch
import numpy as np
from torch.autograd import Variable
import modules.utils as ut




class GraphConv3D(nn.Module):
    def __init__(self, edge_module, message_module, combine_module, update_module):
        super().__init__()
        self.edge_module = edge_module
        self.message_module = message_module
        self.combine_module = combine_module
        self.update_module = update_module

    def forward(self, x):
        # nmask: nBatch, nA, 1 (mask for used atoms)
        # nodes: nBatch, nA+1, nF
        # emask: nBatch, nE+1, 1 (mask for used edges)
        # edges: nBatch, nE+1, nF

        # nmask, nodes, emask, edges, neig1, neig2, neig3, edges_neig = x
        nmask, nodes, emask, edges, n1, e1, g1, n2, e2, g2, n3, e3, g3, edges_neig = x
        paths = [
            [n1, e1, g1,],
            [n2, e2, g2,],
            [n3, e3, g3,],
        ]

        edges = self.edge_module(  nmask, nodes, emask, edges, edges_neig )
        messages = self.message_module( nmask, nodes, emask, edges, paths )
        combined_messages = self.combine_module( messages, n1, n2, n3 )
        nodes = self.update_module( nodes, combined_messages )

        return nmask, nodes, emask, edges, n1, e1, g1, n2, e2, g2, n3, e3, g3, edges_neig

# helper function
def add_dim(t, dim=-1, length=1):
    dims = [-1]*(len(t.shape)+1)
    dims[dim] = length
    return t.unsqueeze(dim).expand(dims)

# Edge Module
class EdgeUpdate(nn.Module):
    def __init__(self, node_dim, edge_dim, out_dim):
        super().__init__()
        self.linear = nn.Sequential(
            nn.Linear(node_dim+edge_dim, out_dim),
            nn.Tanh()
        )
        self.attention = Attention(out_dim, 2, mean=False)

    def forward(self, nmask, nodes, emask, edges, edges_neig):
        # size
        ndim = nodes.shape[2]
        num_edges = edges.shape[1]

        end_nodes = torch.gather(add_dim(nodes, 1, num_edges), 2, add_dim(edges_neig, -1, ndim))
        end_nodes_edge = torch.cat([end_nodes, add_dim(edges, 2, 2)], dim=-1)
        end_nodes_edge = self.linear(end_nodes_edge)
        return self.attention(end_nodes_edge, add_dim(emask, 2, 2))


# Message Modules
class HighOrderMessage(nn.Module):
    "Index lookup style"
    def __init__(self, node_dim, edge_dim, out_dim, geom_bins=16):
        super().__init__()
        self.lin1 = nn.Sequential(
            nn.Linear(2*node_dim+edge_dim+geom_bins, out_dim),
            nn.Tanh() )
        self.lin2 = nn.Sequential(
            nn.Linear(3*node_dim+2*edge_dim+3*geom_bins, out_dim),
            nn.Tanh() )
        self.lin3 = nn.Sequential(
            nn.Linear(4*node_dim+3*edge_dim+6*geom_bins, out_dim),
            nn.Tanh() )

        self.geom_bins = geom_bins

    def forward(self, nmask, nodes, emask, edges, paths):
        num_nodes = nodes.shape[1]
        dim_nodes = nodes.shape[2]
        dim_edges = edges.shape[2]

        features = []
        for order in range(3):
            n_nodes, n_edges, n_geoms = paths[order]
            dim_neigh = list(n_nodes.shape[:3])
            path_length = n_nodes.shape[-1]

            n_nodes_ft = torch.gather(
                add_dim(add_dim(nodes, 1, num_nodes), -1, path_length), 2,
                add_dim(n_nodes, 3, dim_nodes)).permute(0,1,2,4,3).contiguous()
            n_nodes_ft = n_nodes_ft.view(dim_neigh+[-1])

            n_edges_ft = torch.gather(
                add_dim(add_dim(edges, 1, num_nodes), -1, path_length), 2,
                add_dim(n_edges, 3, dim_edges)).permute(0,1,2,4,3).contiguous()
            n_edges_ft = n_edges_ft.view(dim_neigh+[-1])

            n_geoms_ft = ut.soft_1hot(n_geoms.unsqueeze(-1), self.geom_bins, 0.5, 1.8, 
                mask = (n_nodes[:, :, :, :1] > 0).unsqueeze(-1))
            n_geoms_ft = n_geoms_ft.view(dim_neigh + [-1])

            n_features = torch.cat([
                add_dim(nodes, 2, dim_neigh[-1]),
                n_nodes_ft, n_edges_ft, n_geoms_ft
            ], dim=-1)
            features.append(n_features)
            
        return self.lin1(features[0]), self.lin2(features[1]), self.lin3(features[2])

# Combine high order messages
class CombineHighOrderMessages(nn.Module):
    def __init__(self, message_dim):
        super().__init__()
        self.att1 = Attention(message_dim, 2, mean=False)
        self.att2 = Attention(message_dim, 2, mean=False)
        self.att3 = Attention(message_dim, 2, mean=False)

    def forward(self, messages, neig1, neig2, neig3):
        combined1 = self.att1(messages[0], neig1[:, :, :, :1] > 0)
        combined2 = self.att2(messages[1], neig2[:, :, :, :1] > 0)
        combined3 = self.att3(messages[2], neig3[:, :, :, :1] > 0)
        return combined1, combined2, combined3



class GraphDropout(nn.Module):
    def __init__(self, p):
        super().__init__()
        self.dropout = nn.Dropout(p=p)
    def forward(self, x):
        x = list(x)
        x[1] = self.dropout(x[1])
        x[3] = self.dropout(x[3])
        return x

## combination modules ##
class Mean(nn.Module):
    def __init__(self, dim):
        super(Mean, self).__init__()
        self.dim = dim

    def forward(self, messages, emask, nmask):
        # messages: to N, nA, nA, features
        agg = torch.sum( emask.float() * messages, dim=self.dim )
        num = torch.sum(emask, dim=self.dim).float() + (1-nmask.float())
        mean = nmask.float()*(agg/num)
        return mean

class Attention(nn.Module):
    def __init__(self, in_dim, dim, mean=False):
        super().__init__()
        self.projection = nn.Linear(in_dim, 1, bias = False)
        self.dim = dim
        self.mean = mean

    def forward(self, sets, mask=None):
        e = self.projection(sets)
        e = e + (mask.float()-1)*1e10
        a = nn.Softmax(dim=self.dim)(e)
        a = a* mask.float()

        if self.mean:
            agg = torch.sum( a * sets, dim=self.dim )
            num = torch.sum(mask, dim=self.dim).float() + 1e-18 # need better or just use hack?
            mean = nmask.float()*(agg/num)
            return mean
        else:
            return torch.sum( a * sets, dim=self.dim )

class Set2Vec(nn.Module):
    def __init__(self, in_dim, out_dim, dim, steps = 2):
        super().__init__()
        assert out_dim > in_dim
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.lstm = nn.LSTM(self.out_dim, self.out_dim-in_dim, 1)
        self.projection = nn.Linear(self.out_dim, 1, bias = False)
        self.dim = dim
        self.steps = steps

    def forward(self, sets, nmask):
        set_sizes = sets.size()
        num_atoms = sets.size()[1]
        qs = torch.zeros((set_sizes[0], 1, self.out_dim), device=sets.device)

        hidden = (
            torch.zeros((1, 1, self.out_dim-self.in_dim), device=sets.device), 
            torch.zeros((1, 1, self.out_dim-self.in_dim), device=sets.device), 
        )

        for step in range(self.steps):
            q, hidden = self.lstm(qs, hidden)
            expand_q = q.expand(-1, num_atoms, -1)
            
            e = self.projection(torch.cat([sets, expand_q], dim=-1))
            e = e + (nmask.float()-1)*1e10
            a = nn.Softmax(dim=self.dim)(e)
            a = a * nmask.float()
            r = torch.sum( a * sets, dim=self.dim )
            qs = torch.cat([q, r.unsqueeze(1)], dim=-1)

        qs = qs.squeeze(1)
        return qs


class Vec2Set(nn.Module):
    def __init__(self, in_dim, out_dim, expand=29):
        super().__init__()
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.rnn = nn.RNN(self.in_dim, self.out_dim, 1, nonlinearity='tanh')
        self.expand = expand

    def forward(self, x_input):
        h = torch.zeros((1, x_input.shape[0], self.out_dim), device=x_input.device)

        x = x_input.unsqueeze(0).expand(self.expand, -1, -1)
        nodes, h = self.rnn(x, h)
        nodes = nodes.permute(1, 0, 2).contiguous()

        return nodes




## Update Modules ##
class NNUpdate(nn.Module):
    def __init__(self, node_dim, message_dim, out_dim):
        super(NNUpdate, self).__init__()
        self.linear = nn.Sequential(
            nn.Linear(node_dim+message_dim*3, out_dim),
            nn.Tanh()
        )

    def forward(self, nodes, messages):
        features = torch.cat([nodes]+list(messages), dim=-1)
        return self.linear(features)

class GRUUpdate(nn.Module):
    def __init__(self, node_dim, hidden_dim):
        super().__init__()
        self.gru = nn.GRUCell(node_dim, hidden_dim)
        self.ndim = node_dim
        self.hdim = hidden_dim

    def forward(self, nodes, messages):

        nB, nA, _ = nodes.shape

        out = 0
        for i in range(3):
            out = self.gru(nodes.view(nB*nA, -1), out+messages[i].view(nB*nA, -1))
        return out.view(nB, nA, -1)


# class GRUUpdate2(nn.Module):
#     def __init__(self, node_dim, hidden_dim):
#         super().__init__()
#         self.gru = nn.GRUCell(node_dim, hidden_dim)
#
#     def forward(self, nodes, messages):
#         nB, nA, _ = nodes.shape
#         out = self.gru(nodes.view(nB*nA, -1), messages.view(nB*nA, -1))
#         return out.view(nB, nA, -1)


## obsolete
# class Set2Set(nn.Module):
#
#     def __init__(self, in_dim, out_dim, dim, steps = 2):
#         super(Set2Set, self).__init__()
#         assert out_dim > in_dim
#         self.out = out_dim
#         # self.lstm = nn.Sequential(
#         #     nn.Linear(self.total_dim, out_dim-in_dim),
#         #     nn.Tanh(), )
#         self.lstm = nn.LSTM(self.total_dim+in_dim, self.total_dim, 1)
#         self.projection = nn.Linear(self.total_dim, 1, bias = False)
#         self.dim = dim
#         self.steps = steps
#
#     def forward(self, sets):
#         set_sizes = sets.size()
#         num_atoms = sets.size()[1]
#         qs = Variable(sets.data.new(set_sizes[0], self.total_dim).zero_())
#
#         hidden = (
#             torch.zeros((set_sizes[0], self.to), 
#         )
#
#         rnn = nn.LSTM(10, 20, 2)
#         inp = torch.randn(5, 3, 10)
#         h0 = torch.randn(2, 3, 20)
#         c0 = torch.randn(2, 3, 20)
#         output, (hn, cn) = rnn(inp, (h0, c0))
#
#         print(output.shape)
#
#         exit()
#
#         for step in range(self.steps):
#             q = self.lstm(qs)
#             expand_q = q.unsqueeze(1).expand(-1, num_atoms, -1)
#             
#             e = self.projection(torch.cat([sets, expand_q], dim=-1))
#             a = nn.Softmax(dim=self.dim)(e)
#             r = torch.sum( a * sets, dim=self.dim )
#             qs = torch.cat([q, r], dim=-1)
#
#         exit()
#         return qs

# class NodeEdgeNode(nn.Module):
#     "Fast Style"
#     def __init__(self, node_dim, edge_dim, out_dim):
#         super().__init__()
#         self.linear = nn.Sequential(
#             nn.Linear(2*node_dim+edge_dim, out_dim),
#             nn.Tanh()
#         )
#
#     def forward(self, nodes, ngeom, edges, geoms):
#         # feature graph:  nodes-edge-node
#         num_atoms = nodes.size()[1]
#         top_nodes = nodes.unsqueeze(2).expand(-1, -1, num_atoms, -1)
#         bot_nodes = nodes.unsqueeze(1).expand(-1, num_atoms, -1, -1)
#         features = torch.cat((top_nodes, edges, bot_nodes), dim = 3)
#         return self.linear( features )
#
# class NodeEdgeNode3D(nn.Module):
#     "Fast Style"
#     def __init__(self, node_dim, edge_dim, out_dim, angle_bins=16):
#         super().__init__()
#         self.linear = nn.Sequential(
#             nn.Linear(2*node_dim+edge_dim+2*angle_bins, out_dim),
#             nn.Tanh()
#         )
#         self.angle_bins = angle_bins
#
#     def forward(self, nodes, ngeom, edges, geoms):
#         # feature graph:  node-node-geom-theta-alpha
#         nA = nodes.size()[1]
#
#         tensors = []
#         tensors.append(nodes.unsqueeze(2).expand(-1, -1, nA, -1))
#         tensors.append(nodes.unsqueeze(1).expand(-1, nA, -1, -1))
#         tensors.append(edges)
#
#         nvect = ngeom.unsqueeze(2).expand(-1, -1, nA, -1)
#         theta = torch.sum(nvect * geoms[:, :, :, :3], dim=-1)
#         t_1hot = ut.soft_1hot(theta.unsqueeze(-1), self.angle_bins, -1, 1)
#         tensors.append(t_1hot)
#         # tensors.append(theta.unsqueeze(-1))
#
#         torque = torch.cross(nvect, geoms[:, :, :, :3], dim=-1)
#         alpha = torch.sum(torque * torque.transpose(1, 2), dim=-1)
#         a_1hot = ut.soft_1hot(alpha.unsqueeze(-1), self.angle_bins, -1, 1)
#         tensors.append(a_1hot)
#         # tensors.append(alpha.unsqueeze(-1))
#
#         # need checking whether the cross and dot is doing what we want
#         features = torch.cat(tensors, dim = 3)
#         return self.linear( features )
#
# # maybe need a new edge model
# class EdgeMatrixMessages(nn.Module): # mpnn style
#     "Fast Style"
#     def __init__(self, node_dim, edge_dim, out_dim, angle_bins=16):
#         super().__init__()
#         self.linear = nn.Sequential(
#             nn.Linear(edge_dim+2*angle_bins, out_dim*node_dim),
#             nn.Tanh()
#         )
#         self.angle_bins = angle_bins
#         self.node_dim = node_dim
#
#     def forward(self, nodes, ngeom, edges, geoms):
#         # feature graph:  node-node-geom-theta-alpha
#         nA = nodes.size()[1]
#
#         tensors = []
#
#         tensors.append(edges)
#         nvect = ngeom.unsqueeze(2).expand(-1, -1, nA, -1)
#
#         theta = torch.sum(nvect * geoms[:, :, :, :3], dim=-1)
#         t_1hot = ut.soft_1hot(theta.unsqueeze(-1), self.angle_bins, -1, 1)
#         tensors.append(t_1hot)
#
#         torque = torch.cross(nvect, geoms[:, :, :, :3], dim=-1)
#         alpha = torch.sum(torque * torque.transpose(1, 2), dim=-1)
#         a_1hot = ut.soft_1hot(alpha.unsqueeze(-1), self.angle_bins, -1, 1)
#         tensors.append(a_1hot)
#
#         edge_features = torch.cat(tensors, dim = 3)
#
#         # multiplication
#         em_sizes = edge_features.shape[:3]
#         edge_matrix = self.linear(edge_features).view(*em_sizes, -1, self.node_dim) / self.node_dim
#         neig_nodes = nodes.unsqueeze(1).expand(-1, nA, -1, -1).unsqueeze(-2)
#         messages = torch.sum(edge_matrix*neig_nodes, dim=-1)
#         return messages

# class EdgePassThrough(nn.Module):
#     def __init__(self):
#         super(EdgePassThrough, self).__init__()
#     def forward(self, nodes, ngeom, edges, geoms):
#         return edges
#     
