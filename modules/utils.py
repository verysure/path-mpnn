import torch

def one_hot_scalar(t, length, min_v, max_v, mask=None, dim=-1):
    if mask is None:
        m = 1.
    else:
        m = mask.float()

    t_idx = (t.clamp(min=min_v, max=max_v)-min_v) / (max_v - min_v) * (length-1)
    t_idx = t_idx.long()

    dims = [-1] * len(t.shape)
    dims[dim] = length
    t_1hot = torch.zeros_like(t).expand(*dims)
    t_1hot.scatter_(3, t_idx, 1)
    return t_1hot * m

def soft_1hot(t, length, min_v, max_v, mask=None, sig=None, dim=-1):
    if sig is None:
        sig = (max_v-min_v)/length/3.
    if mask is None:
        m = 1.
    else:
        m = mask.float()

    dims = [-1] * len(t.shape)
    dims[dim] = length
    t_1hot = torch.ones_like(t).expand(*dims)

    l_dims = [1] * len(t.shape)
    l_dims[dim] = length
    t_linr = torch.linspace(min_v, max_v, length, device=t.device).view(*l_dims)

    t_1hot = t_1hot * t_linr * m - t.clamp(min=min_v, max=max_v)
    t_1hot = torch.exp(-0.5*t_1hot.pow(2)/sig**2)

    # normalize
    tsum = t_1hot.sum(dim=-1).unsqueeze(-1) + (1-m)
    t_1hot = t_1hot / tsum
    t_1hot = t_1hot*m
    return t_1hot

