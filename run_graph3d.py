import os

import torch
from torch import nn
import modules.moldataset as md
import modules.graph3d as g3d
import modules.utils as ut
from tqdm import tqdm
import numpy as np

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
eh = 27.21138602
unit_convert = np.array([1, 1, 1, 1, 1, eh, eh, eh, 1, eh, eh, eh, eh, eh, 1])
chem_accuracy = np.array([1, 1, 1, .1, .1, .043, .043, .043, 1.2, .0012, .043, .043, .043, .043, .050])
np.random.seed(17)



# For QM9
class SimpleGraphConv(nn.Module):
    def __init__(self, node_dim, edge_dim, out_dim):
        super().__init__()
        self.graph_conv = g3d.GraphConv3D(
            # edge_module = g3d.EdgePassThrough(),
            # edge_module = g3d.NodeEdgeNode(node_dim, edge_dim, out_dim),
            edge_module = g3d.EdgeUpdate(node_dim, edge_dim, out_dim),
            # edge_module = g3d.EdgeMatrixMessages(node_dim, edge_dim, out_dim),

            message_module = g3d.HighOrderMessage(node_dim, out_dim, out_dim),
            # message_module = g3d.NodeEdgeNode3D(node_dim, edge_dim, out_dim),
            # message_module = g3d.NodeEdgeNode(node_dim, edge_dim, out_dim),
            # message_module = g3d.EdgePassThrough(),

            # combine_module = g3d.Attention(out_dim, dim = 2, mean=False),
            combine_module = g3d.CombineHighOrderMessages(out_dim),

            update_module = g3d.NNUpdate(node_dim, out_dim, out_dim),
            # update_module = g3d.GRUUpdate(node_dim, out_dim),
            )
    def forward(self, X):
        return self.graph_conv(X)


class SimpleModel(nn.Module):
    def __init__(self, node_dim, edge_dim, targets = 15, dist_encoding = None):
        super().__init__()

        self.dist_enc = dist_encoding
        edge_dim = edge_dim + dist_encoding[0]

        # with no edge update
        # self.graphconv = nn.Sequential(
        #     SimpleGraphConv(node_dim, edge_dim, 64),
        #     SimpleGraphConv(64, edge_dim, 128),
        #     SimpleGraphConv(128, edge_dim, 128),
        #     SimpleGraphConv(128, edge_dim, 256),
        #     SimpleGraphConv(256, edge_dim, 256),
        # )

        # with edge update
        self.graphconv = nn.Sequential(
            SimpleGraphConv(node_dim, edge_dim, 64),
            SimpleGraphConv(64, 64, 64),
            SimpleGraphConv(64, 64, 128),
            SimpleGraphConv(128, 128, 128),)
        hidden_size = 512
        self.readout = g3d.Set2Vec(128, hidden_size, dim=1, steps=5)
        self.out = nn.Sequential(nn.Dropout(0.2), nn.Linear(hidden_size, targets),)

    def forward(self, x):
        # nmask, nodes, emask, edges, neig1, neig2, neig3, edges_neig = x

        # dists = ut.one_hot_scalar(geoms[:, :, :, -1:], *self.dist_enc, mask=emask)
        # dists = ut.soft_1hot(geoms[:, :, :, -1:], *self.dist_enc, mask=emask)
        # ngeom = torch.sum(geoms[:, :, :, :3] * emask.float(), dim=2) # consider mean for vects

        # do we need to add the distance here?
        # edges = torch.cat([edges, dists], dim=-1)


        # h = [nmask, nodes, ngeom, emask, edges, geoms]
        h = self.graphconv(x)
        h = self.readout(h[1], h[0])

        return self.out(h)



# load data
# folder_path = '../slurmtest/graph/qm8_3d_test'
folder_path = '../slurmtest/graph/qm8_path_test'
if not os.path.isdir(folder_path):
    os.makedirs(folder_path)
root_folder = os.path.expanduser('~/data/qm9')
dist_enc = (0, 0.5, 1.8)
# load_data = False
load_data = True

## qm9
# target_idx = [5] # HOMO
# # target_idx = [10] # U0
# # target_idx = [11] # U289
# mol_ds = md.QM9(root_folder, node_padding=30, edge_padding=26, target_idx = target_idx, normalization=True)
# valid_len, test_len, total_len = 10000, 10000, len(mol_ds)

## qm8
target_idx = list(range(16))
# target_idx = [1]
mol_ds = md.QM8(root_folder, node_padding=27, edge_padding=26, target_idx = target_idx, normalization=True)
valid_len, test_len, total_len = 2000, 2000, len(mol_ds)

## dataloader
idx = np.random.permutation(len(mol_ds))
valid_idx = idx[:valid_len]
test_idx  = idx[valid_len:valid_len+test_len]
train_idx = idx[valid_len+test_len:]

dl_kwargs = dict(batch_size = 128, shuffle = True, num_workers = 16)
train_dl = md.mol_data_loader(mol_ds, idx=train_idx, **dl_kwargs)
valid_dl = md.mol_data_loader(mol_ds, idx=valid_idx, **dl_kwargs)
test_dl  = md.mol_data_loader(mol_ds, idx=test_idx, **dl_kwargs)
data_std = torch.tensor(mol_ds.std[target_idx]).float().unsqueeze(0).to(device)


model = SimpleModel(node_dim=5, edge_dim=4, targets = len(target_idx), dist_encoding=dist_enc)
if load_data:
    model.load_state_dict(torch.load(os.path.join(folder_path, 'weights.pt')))
model.to(device)
opt = torch.optim.Adam(model.parameters(), lr=1e-4)
if load_data:
    opt.load_state_dict(torch.load(os.path.join(folder_path, 'optim.pt')))
mse_loss = nn.MSELoss(reduction='sum')
l1_loss = nn.L1Loss(reduction='none')
acc = lambda y_, y: torch.abs(y_ - y).sum().detach()



def train(epoch, lr):

    for g in opt.param_groups:
        g['lr'] = lr

    train_len = len(train_dl.dataset)
    desc_text = "epoch: {}, lr: {:.1e}, tr_loss: {:.3e}, va_loss: {:.3e}, {:.3e}"
    tbar = tqdm(desc = desc_text.format(epoch, lr, 0, 0, 0), total = train_len)

    train_loss = 0.
    train_samples = 0

    model.train()
    for X, y in train_dl:
        X = [x.to(device) for x in X]
        y = y.to(device)

        opt.zero_grad()
        y_p = model(X)
        loss = mse_loss(y_p, y)
        loss.backward()
        opt.step()

        train_loss += loss.item()
        train_samples += len(y)

        tbar.set_description(desc_text.format(epoch, lr, 
            (train_loss/train_samples/len(target_idx))**0.5, 0, 0) )
        tbar.update(len(y))

    valid_loss = 0.
    valid_mse = 0.
    valid_samples = 0
    model.eval()
    for X, y in valid_dl:
        X = [x.to(device) for x in X]
        y = y.to(device)
        y_p = model(X)

        loss = mse_loss(y_p, y).detach()
        valid_mse += loss.item() 

        loss = l1_loss(y_p, y).detach()
        # valid_loss += loss.sum().item() # without normalization
        valid_loss += ( loss*data_std ).sum().item() # with
        # valid_loss += (loss*data_std*eh/0.043).sum().item() # qm9
        valid_samples += len(y)

        tbar.set_description(desc_text.format(epoch, lr, (train_loss/train_samples/len(target_idx))**0.5, 
            (valid_mse/valid_samples/len(target_idx))**0.5, valid_loss/valid_samples/len(target_idx)) )

    tbar.close()
    torch.save(model.state_dict(), os.path.join(folder_path, 'weights.pt'))
    torch.save(opt.state_dict(), os.path.join(folder_path, 'optim.pt'))
    with open(os.path.join(folder_path, 'logfile'), 'a') as f:
        f.write(desc_text.format(epoch, lr, (train_loss/train_samples/len(target_idx))**0.5, 
            (valid_mse/valid_samples/len(target_idx))**0.5, valid_loss/valid_samples/len(target_idx)) )
        f.write('\n')

    
# qm9
# N1, N2, N3 = 30, 100, 300
# for epoch in range(N1):
#     train(epoch, lr=1e-4)
#
# for epoch, lr in zip(range(N1, N2), np.linspace(1e-4, 1e-5, N2-N1)):
#     train(epoch, lr)
#
# lr_str, lr_end = 1e-5, 1e-8
# for epoch, i in zip(range(N2, N3), np.linspace(0, 1, N3-N2)):
#     lr = lr_str**(1-i) * lr_end**(i)
#     train(epoch, lr)


# qm8
# N1, N2, N3 = 400, 750, 800
# for epoch in range(N1):
#     train(epoch, lr=1e-4)
#
# for epoch, lr in zip(range(N1, N2), np.linspace(1e-4, 1e-5, N2-N1)):
#     train(epoch, lr)
#
# lr_str, lr_end = 1e-5, 1e-7
# for epoch, i in zip(range(N2, N3), np.linspace(0, 1, N3-N2)):
#     lr = lr_str**(1-i) * lr_end**(i)
#     train(epoch, lr)

def evaluate(dataset_dl):
    model.eval()
    dl_loss = 0.
    dl_samples = 0
    for X, y in dataset_dl:
        X = [x.to(device) for x in X]
        y = y.to(device)
        y_p = model(X)

        loss = l1_loss(y_p, y).detach()
        # dl_loss += loss.sum().item() # without normalization
        dl_loss += ( loss*data_std ).sum().item() # with
        # dl_loss += (loss*data_std*eh/0.043).sum().item() # qm9
        dl_samples += len(y)
    return dl_loss/dl_samples/len(target_idx)

with open(os.path.join(folder_path, 'results'), 'w') as f:
    f.write('Validate: {:e}\n'.format(evaluate(valid_dl)))
    f.write('Test: {:e}\n'.format(evaluate(test_dl)))




