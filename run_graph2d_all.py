import os

import torch
from torch import nn
import modules.moldataset as md
import modules.graph3d as g3d
import modules.utils as ut
from tqdm import tqdm
import numpy as np

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
eh = 27.21138602
unit_convert = np.array([1, 1, 1, 1, 1, eh, eh, eh, 1, eh, eh, eh, eh, eh, 1])
chem_accuracy = np.array([1, 1, 1, .1, .1, .043, .043, .043, 1.2, .0012, .043, .043, .043, .043, .050])

# helpers
# def one_hot_scalar(t, m, length, min_v, max_v):
#     t_idx = (t.clamp(min=min_v, max=max_v)-min_v) / (max_v - min_v) * (length-1)
#     t_idx = t_idx.long()
#
#     t_1hot = torch.zeros((t.shape[0], t.shape[1], t.shape[2], length), 
#         device = t_idx.device)
#     t_1hot.scatter_(3, t_idx, 1)
#
#     return t_1hot * m.float()

# For QM9
class SimpleGraphConv(nn.Module):
    def __init__(self, node_dim, edge_dim, out_dim):
        super().__init__()
        self.graph_conv = g3d.GraphConv3D(
            # edge_module = g3d.EdgePassThrough(),
            # edge_module = g3d.NodeEdgeNode(node_dim, edge_dim, out_dim),
            edge_module = g3d.NodeEdgeNode(node_dim, edge_dim, out_dim),
            # edge_module = g3d.EdgeMatrixMessages(node_dim, edge_dim, out_dim),

            # message_module = g3d.NodeEdgeNode3D(node_dim, edge_dim, out_dim),
            # message_module = g3d.NodeEdgeNode(node_dim, edge_dim, out_dim),
            message_module = g3d.EdgePassThrough(),

            # combine_module = g3d.Attention(out_dim, dim = 2, mean=False),
            combine_module = g3d.Mean(dim = 2),

            # update_module = g3d.NNUpdate(node_dim, out_dim, out_dim),
            update_module = g3d.GRUUpdate(node_dim, out_dim),
            )
    def forward(self, X):
        return self.graph_conv(X)


class SimpleModel(nn.Module):
    def __init__(self, node_dim, edge_dim, targets = 15, dist_encoding = None):
        super().__init__()

        self.dist_enc = dist_encoding
        edge_dim = edge_dim + dist_encoding[0]

        # with no edge update
        # self.graphconv = nn.Sequential(
        #     SimpleGraphConv(node_dim, edge_dim, 64),
        #     SimpleGraphConv(64, edge_dim, 128),
        #     SimpleGraphConv(128, edge_dim, 128),
        #     SimpleGraphConv(128, edge_dim, 256),
        #     SimpleGraphConv(256, edge_dim, 256),
        # )

        # with edge update
        self.graphconv = nn.Sequential(
            SimpleGraphConv(node_dim, edge_dim, 64), #g3d.GraphDropout(0.1),
            SimpleGraphConv(64, 64, 64), # g3d.GraphDropout(0.1),

            SimpleGraphConv(64, 64, 128), #g3d.GraphDropout(0.2),
            SimpleGraphConv(128, 128, 128), #g3d.GraphDropout(0.1),

            SimpleGraphConv(128, 128, 256), #g3d.GraphDropout(0.3),
            SimpleGraphConv(256, 256, 256), #g3d.GraphDropout(0.2),
        )

        # self.graphconv = nn.Sequential(
        #     SimpleGraphConv(node_dim, edge_dim, 64), #g3d.GraphDropout(0.1),
        #     SimpleGraphConv(64, 64, 64), #g3d.GraphDropout(0.1),
        #     SimpleGraphConv(64, 64, 64), g3d.GraphDropout(0.1),
        #
        #     SimpleGraphConv(64, 64, 128), #g3d.GraphDropout(0.2),
        #     SimpleGraphConv(128, 128, 128), #g3d.GraphDropout(0.1),
        #     SimpleGraphConv(128, 128, 128), g3d.GraphDropout(0.2),
        #
        #     SimpleGraphConv(128, 128, 256), #g3d.GraphDropout(0.3),
        #     SimpleGraphConv(256, 256, 256), g3d.GraphDropout(0.2),
        # )

        # self.graphconv = nn.Sequential(
        #     SimpleGraphConv(node_dim, edge_dim, 64),
        #     SimpleGraphConv(64, 64, 64),
        #
        #     SimpleGraphConv(64, 64, 128),
        #     SimpleGraphConv(128, 128, 128),
        # )

        # self.graphconv = nn.Sequential(
        #     SimpleGraphConv(node_dim, edge_dim, 32),
        #     SimpleGraphConv(32, 32, 32),
        #
        #     SimpleGraphConv(32, 32, 64),
        #     SimpleGraphConv(64, 64, 128),
        # )

        hidden_size = 1024
        self.readout = g3d.Set2Vec(256, hidden_size, dim=1, steps=5)
        self.out = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(hidden_size, targets),
        )

    def forward(self, x):
        nmask, nodes, emask, edges, geoms = x
        # dists = ut.one_hot_scalar(geoms[:, :, :, -1:], *self.dist_enc, mask=emask)
        dists = ut.soft_1hot(geoms[:, :, :, -1:], *self.dist_enc, mask=emask)

        ngeom = torch.sum(geoms[:, :, :, :3] * emask.float(), dim=2) # consider mean for vects
        edges = torch.cat([edges, dists], dim=-1)

        # select all distances
        # print(emask.shape, nmask.shape)
        nA = emask.shape[1]
        emask = (nmask.unsqueeze(1) * nmask.unsqueeze(2))
        emask[:, range(nA), range(nA), :] = 0

        h = [nmask, nodes, ngeom, emask, edges, geoms]
        h = self.graphconv(h)
        h = self.readout(h[1], nmask)
        return self.out(h)





# load data
# target_idx = [5, 6, 7]
folder_path = '../slurmtest/graph/model2d_all'
target_idx = [5]
qm9_root = os.path.expanduser('~/data/qm9')
mol_ds = md.QM9(qm9_root, padding_size = 29, target_idx = target_idx, normalization=True)
valid_len, test_len, total_len = 10000, 10000, len(mol_ds)
splits = [total_len-test_len-valid_len, test_len, valid_len]
train_dl, valid_dl, test_dl = md.mol_data_loader(
    mol_ds, splits=splits, batch_size = 128, shuffle = True, num_workers = 12)

# one hot distance
dist_enc = (12, 0.5, 2.0)

model = SimpleModel(node_dim=5, edge_dim=4, targets = len(target_idx), dist_encoding=dist_enc)
# model.load_state_dict(torch.load(os.path.join(folder_path, 'weights.pt')))
model.to(device)
opt = torch.optim.Adam(model.parameters(), lr=1e-4)
# opt.load_state_dict(torch.load(os.path.join(folder_path, 'optim.pt')))
mse_loss = nn.MSELoss(reduction='sum')
l1_loss = nn.L1Loss(reduction='sum')
acc = lambda y_, y: torch.abs(y_ - y).sum().detach()

def train(epoch, lr):

    for g in opt.param_groups:
        g['lr'] = lr

    train_len = len(train_dl.dataset)
    desc_text = "epoch: {}, lr: {:.1e}, tr_loss: {:.3f}, va_loss: {:.3f}, {:.3f}"
    tbar = tqdm(desc = desc_text.format(epoch, lr, 0, 0, 0), total = train_len)

    train_loss = 0.
    train_samples = 0

    model.train()
    for X, y in train_dl:
        X = [x.to(device) for x in X]
        y = y.to(device)

        opt.zero_grad()
        y_p = model(X)
        loss = mse_loss(y_p, y)
        loss.backward()
        opt.step()

        train_loss += loss.item() * (mol_ds.std[target_idx].item() * eh)**2
        train_samples += len(y)

        tbar.set_description(desc_text.format(epoch, lr, 
            (train_loss/train_samples)**0.5/0.043, 0, 0) )
        tbar.update(len(y))

    valid_loss = 0.
    valid_mse = 0.
    valid_samples = 0
    model.eval()
    for X, y in valid_dl:
        X = [x.to(device) for x in X]
        y = y.to(device)
        y_p = model(X)

        loss = l1_loss(y_p, y)
        valid_loss += loss.item() * (mol_ds.std[target_idx].item() * eh)

        loss = mse_loss(y_p, y).detach()
        valid_mse += loss.item() * (mol_ds.std[target_idx].item() * eh)**2

        valid_samples += len(y)
        tbar.set_description(desc_text.format(epoch, lr, (train_loss/train_samples)**0.5/0.043, 
            (valid_mse/valid_samples)**0.5/0.043, valid_loss/valid_samples/0.043) )

    tbar.close()
    torch.save(model.state_dict(), os.path.join(folder_path, 'weights.pt'))
    torch.save(opt.state_dict(), os.path.join(folder_path, 'optim.pt'))
    with open(os.path.join(folder_path, 'logfile'), 'a') as f:
        f.write(desc_text.format(epoch, lr, (train_loss/train_samples)**0.5/0.043, 
            (valid_mse/valid_samples)**0.5/0.043, valid_loss/valid_samples/0.043) )
        f.write('\n')
    

N1, N2, N3 = 15, 100, 300
for epoch in range(N1):
    train(epoch, lr=1e-4)

for epoch, lr in zip(range(N1, N2), np.linspace(1e-4, 1e-5, N2-N1)):
    train(epoch, lr)

lr_str, lr_end = 1e-5, 1e-8
for epoch, i in zip(range(N2, N3), np.linspace(0, 1, N3-N2)):
    lr = lr_str**(1-i) * lr_end**(i)
    train(epoch, lr)





