import os

import torch
from torch import nn
from modules import moldataset as md
from modules import graph
from tqdm import tqdm

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# helpers
def one_hot_scalar(t, m, length, min_v, max_v):
    t_idx = (t.clamp(min=min_v, max=max_v)-min_v) / (max_v - min_v) * (length-1)
    t_idx = t_idx.long()

    t_1hot = torch.zeros((t.shape[0], t.shape[1], t.shape[2], length), 
        device = t_idx.device)
    t_1hot.scatter_(3, t_idx, 1)

    return t_1hot * m.float()

# For QM9
class SimpleGraphConv(nn.Module):
    def __init__(self, node_dim, edge_dim, out_dim):
        super().__init__()
        self.graph_conv = graph.GraphConv(
            edge_module = graph.EdgePassThrough(),
            message_module = graph.NodeEdgeNodeLinear(node_dim, edge_dim, out_dim),
            combine_module = graph.Attention(out_dim, dim = 2),
            update_module = graph.NNUpdate(node_dim, out_dim, out_dim),
            )
    def forward(self, nodes_edges):
        nodes, edges = nodes_edges
        return self.graph_conv(nodes, edges)


class SimpleModel(nn.Module):
    def __init__(self, node_dim, edge_dim, targets = 15, dist_encoding = None):
        super().__init__()

        self.dist_enc = dist_encoding
        edge_dim = edge_dim + dist_encoding[0]

        self.gc1 = SimpleGraphConv(node_dim, edge_dim, 32)
        self.gc2 = SimpleGraphConv(32, edge_dim, 32)

        self.readout = graph.Set2Set(32, 512, dim=1)
        self.linear = nn.Linear(512, targets)

    def forward(self, x):
        nmask, nodes, emask, edges, geoms = x

        dists = one_hot_scalar(geoms[:, :, :, -1:], emask, *self.dist_enc)

        edges = torch.cat([edges, dists], dim=-1)
        h = [nodes, edges]
        h = self.gc1(h)
        h = self.gc2(h)
        h = self.readout(h[0])
        return self.linear(h)






# load data
qm9_root = os.path.expanduser('~/data/qm9')
mol_ds = md.MolDataset(qm9_root, padding_size = 29)
train_dl = torch.utils.data.DataLoader(
    mol_ds, batch_size = 64, shuffle = True, num_workers = 4)
# TODO: add test_dl

# one hot distance
dist_enc = (28, 0.5, 1.5)

model = SimpleModel(node_dim=5, edge_dim=4, targets = 15, dist_encoding=dist_enc)
model.to(device)
opt = torch.optim.Adam(model.parameters(), lr=1e-3)
mse_loss = nn.MSELoss()

for X, y in tqdm(train_dl):

    X = [x.to(device) for x in X]
    y = y.to(device)

    opt.zero_grad()

    y_p = model(X)
    loss = mse_loss(y_p, y)
    loss.backward()

    opt.step()



